const expect = require('chai').expect;
const Graph = require('..');

describe("Graph", () => {
  beforeEach(function() { this.graph = Graph(); });

  describe("Graph() constructor", function() {
    it("Creates an empty graph", function() {
      expect(this.graph.nodes.length).to.equal(0);
      expect(this.graph.edges.length).to.equal(0);
    });

    describe("Invoked with an array of nodes", function() {
      beforeEach(function() {
        this.graph = Graph([["foo"], ["bar", "bam"]]);
      });

      it("Creates a graph containing those nodes", function() {
        expect(this.graph.nodes.length).to.equal(2);
        expect(this.graph.edges.length).to.equal(0);

        expect(this.graph.nodes[0]).to.deep.equal(["foo", undefined]);
        expect(this.graph.nodes[1]).to.deep.equal(["bar", "bam"]);
      });
    });
  });

  describe("Modifying data included in a graph node", function() {
    it("should not be reflected in the graph node", function() {
      const data = {bar: 'baz'};
      this.graph = Graph([["foo", data]]);
      data.bar = 'bam';
      expect(this.graph.node('foo')).to.deep.equal({bar: 'baz'});
    });
  });

  describe("Modifying deep data included in a graph node", function() {
    it("should not be reflected in the graph node", function() {
      const data = {bar: {baz: 'bam'}};
      this.graph = Graph([["foo", data]]);
      data.bar.baz = 'quux';
      expect(this.graph.node('foo').bar).to.deep.equal({baz: 'bam'});
    });
  });

  describe("addNode(node, data)", function() {
    it("Returns a new graph with the added node", function() {
      const graph = this.graph.addNode("pizza");
      expect(graph.nodes.length).to.equal(1);
      expect(graph.nodes[0]).to.deep.equal(["pizza", undefined]);

      const graph2 = graph.addNode("crust", {key: "value"});
      expect(graph2.nodes.length).to.equal(2);
      expect(graph.nodes[0]).to.deep.equal(["pizza", undefined]);
      expect(graph2.nodes[1]).to.deep.equal(["crust", {key: "value"}]);
    });

    it("Leaves the original graph unmodified", function() {
      this.graph.addNode("pizza");
      expect(this.graph.nodes.length).to.equal(0);
    });

    it("Replaces an existing node with the same id", function() {
      const graph = this.graph.addNode("crust", {key: "value"});
      expect(graph.nodes.length).to.equal(1);
      expect(graph.nodes[0]).to.deep.equal(["crust", {key: "value"}]);

      const graph2 = graph.addNode("crust", {newKey: "newValue"});
      expect(graph2.nodes.length).to.equal(1);
      expect(graph2.nodes[0]).to.deep.equal(["crust", {newKey: "newValue"}]);
      expect(graph.nodes.length).to.equal(1);
      expect(graph.nodes[0]).to.deep.equal(["crust", {key: "value"}]);
    });
  });

  describe("node(id)", function() {
    it("Returns undefined when there is no node with id", function() {
      expect(this.graph.node("foo")).to.be.undefined;
    });

    it("Returns the data assigned to a node with id", function() {
      const graph = this.graph.addNode("foo", {key: "value"});
      expect(graph.node("foo")).to.deep.equal({key: "value"});
    });
  });

  describe("edge operations", function() {
    describe("graph.addEdge(source, dest, data)", function() {
      it("adds an edge to the graph and assigns it data", function() {
        expect(this.graph.edges.length).to.equal(0);

        const graph = this.graph.addEdge("a", "b");
        expect(graph.edges.length).to.equal(1);
        expect(graph.edges[0]).to.deep.equal(["a", "b", undefined]);

        const graph2 = graph.addEdge("b", "c", {key: "value"});
        expect(graph2.edges.length).to.equal(2);
        expect(graph2.edges[1]).to.deep.equal(["b", "c", {key: "value"}]);
      });

      it("Leaves the original graph unmodified", function() {
        this.graph.addEdge("a", "b");
        expect(this.graph.nodes.length).to.equal(0);
      });

      it("Replaces an existing edge with the same id", function() {
        const graph = this.graph.addEdge("a", "b", {key: "value"});
        expect(graph.edges.length).to.equal(1);
        expect(graph.edges[0]).to.deep.equal(["a", "b", {key: "value"}]);

        const graph2 = graph.addEdge("a", "b", {newKey: "newValue"});
        expect(graph2.edges.length).to.equal(1);
        expect(graph2.edges[0]).to.deep.equal(["a", "b", {newKey: "newValue"}]);
        expect(graph.edges.length).to.equal(1);
        expect(graph.edges[0]).to.deep.equal(["a", "b", {key: "value"}]);
      });
    });

    describe("graph.edge(source, dest)", function() {
      it("Returns undefined when there is no edge", function() {
        expect(this.graph.edge("a", "b")).to.be.undefined;
      });

      it("Returns the data assigned to an edge", function() {
        const graph = this.graph.addEdge("a", "b", {key: "value"});
        expect(graph.edge("a", "b")).to.deep.equal({key: "value"});
      });
    });
  });

  describe("graph.addNodes(nodes)", function() {
    it("Returns a new graph with the added nodes", function() {
      const graph = this.graph.addNodes([["pizza"], ["foo", "bar"]]);
      expect(graph.nodes.length).to.equal(2);
      expect(graph.nodes[0]).to.deep.equal(["pizza", undefined]);
      expect(graph.nodes[0][1]).to.be.undefined;
      expect(graph.nodes[1]).to.deep.equal(["foo", "bar"]);
    });

    it("Leaves the original graph unmodified", function() {
      this.graph.addNodes([["pizza"], ["foo", "bar"]]);
      expect(this.graph.nodes.length).to.equal(0);
    });

    it("Replaces existing nodes with the same id", function() {
      const graph = this.graph
        .addNodes([["pizza"], ["foo", "bar"]])
        .addNodes([["dog"], ["foo", "bam"]]);

      expect(graph.nodes.length).to.equal(3);

      const keys = graph.nodes.map(node => node[0]);
      expect(keys).to.contain("pizza");
      expect(keys).to.contain("foo");
      expect(keys).to.contain("dog");

      const map = new Map(graph.nodes);
      expect(map.get("pizza")).to.be.undefined;
      expect(map.get("foo")).to.equal("bam");
      expect(map.get("dog")).to.be.undefined;
    });
  });

  describe("graph.addEdges(edges)", function() {
    it("Returns a new graph with the added edges", function() {
      const graph = this.graph.addEdges([["a", "b"], ["c", "d", {e: "f"}]]);
      expect(graph.edges.length).to.equal(2);
      expect(graph.edges[0]).to.deep.equal(["a", "b", undefined]);
      expect(graph.edges[0][2]).to.be.undefined;
      expect(graph.edges[1]).to.deep.equal(["c", "d", {e: "f"}]);
    });

    it("Leaves the original graph unmodified", function() {
      this.graph.addNodes([["pizza"], ["foo", "bar"]]);
      expect(this.graph.edges.length).to.equal(0);
    });

    it("Replaces existing edges between the same nodes", function() {
      const graph = this.graph
        .addEdges([["a", "b"], ["b", "c", "oldData"]])
        .addEdges([["d", "e"], ["b", "c", "newData"]]);

      expect(graph.edges.length).to.equal(3);

      // TODO: order shouldn't really matter in this test
      expect(graph.edges).to.deep.equal([
        ["a", "b", undefined],
        ["b", "c", "newData"],
        ["d", "e", undefined]
      ]);
    });
  });
});
