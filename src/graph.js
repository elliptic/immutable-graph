const Immutable = require('immutable');

module.exports = Graph;

// nodes: pairs of [id, data] or immutable Map
// edges: triples of [source, dest, data] or immutable Map
function Graph(nodes, edges) {
  const _nodes = Nodes(nodes);
  const _edges = Edges(edges);

  const _graph = {};

  Object.defineProperty(_graph, "nodes", {get: () => nodesToJS(_nodes)});
  Object.defineProperty(_graph, "edges", {get: () => edgesToJS(_edges)});

  _graph.node = id => valToJS(_nodes.get(id));
  _graph.edge = (from, to) => valToJS(_edges.get(Immutable.List([from, to])));

  _graph.addNode = (id, data) => Graph(
    _nodes.set(id, Immutable.fromJS(data)),
    _edges
  );

  _graph.addEdge = (source, dest, data) => Graph(
    _nodes,
    _edges.set(Immutable.List([source, dest]), Immutable.fromJS(data))
  );

  _graph.addNodes = (nodes) => Graph(
    _nodes.mergeDeep(Immutable.fromJS(nodes)),
    _edges
  );

  _graph.addEdges = (edges) => Graph(
    _nodes,
    _edges.mergeDeep(Immutable.fromJS(triplesToTuples(edges)))
  );

  return _graph;
}

const Nodes = nodes => Immutable.Map(Immutable.fromJS(nodes));

const Edges = edges =>
  Immutable.Iterable.isIterable(edges) ?
    edges :
    Immutable.Map(Immutable.fromJS(triplesToTuples(edges)));

// don't try to call .toJS() on non-Immutable lib stuff
const valToJS = val =>
  Immutable.Iterable.isIterable(val) ?
    val.toJS() :
    val;

const nodesToJS = nodes => mapToTuples(nodes);
const edgesToJS = edges => mapToTriples(edges);

// Map => [[key, val]]
function mapToTuples (map) {
  const obj = map.toJS();
  return Object.keys(obj).map(key => [key, obj[key]]);
}

// Map <List [a, b], val> => [[a, b, val]]
function mapToTriples (map) {
  return Array.from(map.entries()).map(function triplize(keyVal) {
    const key = keyVal[0].toArray();
    const val = Immutable.Iterable.isIterable(keyVal[1]) ?
      keyVal[1].toJS() :
      keyVal[1];
    return [key[0], key[1], val];
  });
}

// [[a, b, c]] => [[[a, b], c]]
function triplesToTuples(triples) {
  if (triples)
    return triples.map(triple =>[[triple[0], triple[1]], triple[2]]);
  else return [];
}
